# strix-web

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```


### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Testing
Uses ava and @vue/test-utils
https://vue-test-utils.vuejs.org/

```
# run ava once
$ npm run test

# run ava on watch 
$ npm run test:watch
```
For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

