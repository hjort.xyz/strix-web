import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/Home.vue';

Vue.use(Router);

const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home,
		},
		{
			path: '/login',
			name: 'login',
			meta: { layout: 'blank'},
			component: () => import('./pages/Login.vue'),
		},
		{
			path: '/calendar',
			name: 'calendar',
			component: () => import('./pages/Calendar.vue'),
		},
		{
			path: '/contacts',
			name: 'contacts',
			component: () => import('./pages/Contacts.vue'),
		},
		{
			path: '/notes',
			name: 'notes',
			component: () => import('./pages/Notes.vue'),
		},
		{
			path: '/settings',
			name: 'settings',
			component: () => import('./pages/Settings.vue'),
		},
		{
			path: '/tasks',
			name: 'tasks',
			component: () => import('./pages/Tasks.vue'),
		},
		{
			path: '/lab',
			name: 'lab',
			component: () => import('./pages/Lab.vue'),
		},
		{
			path: '*',
			redirect: '/',
		},
	],
});

router.beforeEach((to, from, next) => {
	const public_pages = ['/login', '/lab'];
	const auth_required = !public_pages.includes(to.path);
	const logged_in = !!localStorage.getItem('token');

	if (auth_required && !logged_in) {
		return next('/login');
	} else if (logged_in && to.path === '/login') {
		return next('/');
	}

	next();
});

export default router;
