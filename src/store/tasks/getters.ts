import { Task } from '@/types/vuex-task';

export default {
	all_tasks: (state: Task[]): Task[] => state.filter((task: Task) => {
		return !task.deleted;
	}),
	completed_tasks: (state: Task[]): Task[] => state.filter((task: Task) => {
		return !!task.completed && !task.deleted;
	}),
	synced_tasks: (state: Task[]): Task[] => state.filter((task: Task) => {
		return !!task.synced;
	}),
	unsynced_tasks: (state: Task[]): Task[] => state.filter((task: Task) => {
		return !task.synced;
	}),
	never_synced_tasks: (state: Task[]): Task[] => state.filter((task: Task) => {
		return !!task.temp_id;
	}),
	deleted_tasks: (state: Task[]): Task[] => state.filter((task: Task) => {
		return !!task.deleted;
	}),
};
