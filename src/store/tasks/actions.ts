import Vue from 'vue';
import ActionContext from '@/types/vuex-context';
import { Task_state, Task } from '@/types/vuex-task';

export default {
	create_task({ commit }: ActionContext<Task_state>, { title, description, completed }: Task) {
		// Create task
		const task: Task = {
			temp_id: 'temp_' + Date.now(),
			title,
			description,
			completed,
			synced: true,
		};

		// Save to client store
		commit('CREATE_TASK', task);
	},
	toggle_task({ commit }: ActionContext<Task_state>, { id, completed }: Task) {
		// Toggle in local store
		commit('UPDATE_TASK', { id, completed });
	},
	update_task({ commit }: ActionContext<Task_state>, { id, title, description }: Task) {
		// Update local state
		commit('UPDATE_TASK', { id, title, description });
	},
	delete_task({ commit }: ActionContext<Task_state>, { id }: Task) {
		// Delete in local state
		commit('DELETE_TASK', id);
	},
	sync_task({ commit }: ActionContext<Task_state>, { id, task }: any) {
		// Update local state
		commit('SYNC_TASK', { id, task });
	},
	async sync_tasks({ commit, state }: ActionContext<Task_state>) {
		// Check is state is empty
		if (state.length === 0) {
			// Fetch tasks
			const { data: tasks } = await Vue.axios.get('task');
			commit('INIT_TASKS', tasks);
		}
	},
	hard_delete_task({ commit }: ActionContext<Task_state>, id: Task) {
		// Delete in local state
		commit('HARD_DELETE_TASK', id);
	},
};
