import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import VuexPersistence from 'vuex-persist';
import tasks from './tasks';
import auth from './auth';
import network from './network';

const vuex_local = new VuexPersistence({
	storage: window.localStorage,
});

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		tasks,
		auth,
		network,
	},
	plugins: [
		vuex_local.plugin,
	],
} as StoreOptions<any>);
