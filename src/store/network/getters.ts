import { Network_state } from '@/types/vuex-network';

export default {
	network_status: (state: Network_state) => {
		if (state.syncing) {
			return 'downloading';
		} else if (state.online) {
			return 'online';
		} else {
			return 'offline';
		}
	},
};
