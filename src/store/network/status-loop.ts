const status_loop = (vue: any, commit: any, dispatch: any) => async () => {
	try {
		await vue.axios.get('/status');
		commit('MARK_ONLINE');

		dispatch('sync');
	} catch (error) {
		commit('MARK_OFFLINE');
	}
};

export default status_loop;
