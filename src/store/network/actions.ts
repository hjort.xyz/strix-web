import Vue from 'vue';
import ActionContext from '@/types/vuex-context';
import { Network_state } from '@/types/vuex-network';
import status_loop from './status-loop';
import msa from './msa';

export default {
	start_watch({ commit, rootGetters, dispatch }: ActionContext<Network_state>) {
		if (rootGetters.is_authenticated) {
			// Create watch interval
			const interval_id = window.setInterval(status_loop(Vue, commit, dispatch), 10000);

			// Start watch
			commit('START_WATCH', interval_id);
		}
	},
	stop_watch({ commit, state }: ActionContext<Network_state>) {
		window.clearInterval(state.interval_id);
		commit('STOP_WATCH');
	},
	async sync({ commit, rootGetters, dispatch }: ActionContext<Network_state>) {
		// Stop sync for UI indicator
		commit('START_SYNC');

		// Run MSA
		await msa(rootGetters, dispatch, Vue);

		// Stop sync for UI indicator
		commit('STOP_SYNC');
	},
};
