import { Auth_state } from '@/types/vuex-auth';
import _has from 'lodash.has';
import _get from 'lodash.get';

export default {
	is_authenticated: (state: Auth_state) => {
		const user_data = state.user_data;

		// Check that expiration exists and that it is not expired
		if (_has(user_data, 'exp') && _get(user_data, 'exp') > Date.now()) {
			return true;
		}

		// TODO: remove
		if (_has(user_data, 'username')) {
			return true;
		}

		return false;
	},
};
