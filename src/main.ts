import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import Vue2TouchEvents from 'vue2-touch-events';
import axios from 'axios';
import VueAxios from 'vue-axios';

import default_layout from './layouts/default.vue';
import blank_layout from './layouts/blank.vue';

Vue.component('default-layout', default_layout);
Vue.component('blank-layout', blank_layout);

Vue.config.productionTip = false;
Vue.use(Vue2TouchEvents, {
	swipeTolerance: 60,
});
Vue.use(VueAxios, axios);

const dev = process.env.NODE_ENV !== 'production';
Vue.axios.defaults.baseURL = dev ? 'http://localhost:3001' : 'https://api.strix.hjort.xyz';
Vue.axios.defaults.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount('#app');
