// tslint:disable-next-line: class-name
export interface Network_state {
	online: boolean;
	syncing: boolean;
	interval_id?: number;
}
