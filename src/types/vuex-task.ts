export type Task_state = Task[];

export interface Task {
	temp_id?: string;
	id?: string;
	title?: string;
	description?: string;
	completed?: boolean;
	synced?: boolean;
	deleted?: boolean;
}
