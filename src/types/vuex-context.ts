export default interface ActionContext<T> {
	commit: (mutation: string, data?: any) => {};
	dispatch: (action: string, data?: any) => {};
	state: T;
	rootState: any;
	rootGetters: any;
}
