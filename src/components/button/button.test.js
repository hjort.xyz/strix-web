import { shallowMount } from '@vue/test-utils'
import test from 'ava';
import Button from './button.vue';

test('Renders correctly with no props or input', t => {
	const el = shallowMount(Button);
	t.is(el.contains('button'), true);
	t.is(el.classes('primary'), true);
	t.is(el.attributes('title'), undefined);
});

test('Renders correctly with different props', t => {
	const el = shallowMount(Button, {
		propsData: {
			type: "accent",
			title: "A button"
		}
	});
	
	t.is(el.classes('accent'), true);
	t.is(el.attributes('title'), 'A button');
	
	el.setProps({
		type: "ghost",
		title: "The same button"
	});

	t.is(el.classes('ghost'), true);
	t.is(el.attributes('title'), 'The same button');
});

test('Renders correctly with input', t => {
	const el = shallowMount(Button, {
		slots: {
			default: 'Click me'
		}
	});
	t.is(el.text() === 'Click me', true);
});