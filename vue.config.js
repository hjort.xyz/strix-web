// vue.config.js
module.exports = {
	pwa: {
		name: "Strix",
		themeColor: "#FFC107",
		msTileColor: "#FFC107",
		iconPaths: {
			favicon16: 'img/icon.png',
			favicon32: 'img/icon.png',
			appleTouchIcon: 'img/icon.png',
			maskIcon: 'img/icon.png',
			msTileImage: 'img/icon.png'
		}
	},
	devServer: {
		port: 3000,
	  },
}
